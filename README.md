**Libreria para Servicio de Mapas de Cartografia Electoral**

---

## Tecnologias Usadas

Lista de tecnologias usadas.

1. [jQuery](https://jquery.com/) 3.3.1
2. [LeafLet](https://leafletjs.com/) 1.3.3
3. Custom LeafLet Mask

---

## Documentacion y Ejemplos

Descripcion de documentacion y ejemplos.


[Documentacion](https://drive.google.com/drive/folders/1pugkNb6GYUDFV30O_aDPT5-E4Fo_ZrZ7).

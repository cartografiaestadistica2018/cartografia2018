module.exports = function(grunt) {

  // Project configuration.
  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),
    uglify: {
      options: {
        banner: '/*! <%= pkg.name %> <%= grunt.template.today("yyyy-mm-dd") %> */\n'
      },
      build: {
        src: 'src/<%= pkg.name %>.js',
        dest: 'build/<%= pkg.name %>.min.js'
      }
    },
    concat: {
       bar: {
         src: ['resources/js/dev/leaflet-1.3.3.js',
              'resources/js/dev/polyline.encoded.js',
              'resources/js/dev/interfaz.js',
              'resources/js/dev/leaflet-map.js',
              'resources/js/dev/cartografia.js'
               ],
         dest: 'resources/js/cartografia2018.js',
       },
    },
  });

  // Load the plugin that provides the "uglify" task.
  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-contrib-concat');

  // Default task(s).
  grunt.registerTask('default', [ 'concat', 'uglify']);
};

var LeafLetMap = function( __params ){
  var om = {};
  var info = { entidades: {} , distritos : {} };
  var omapo;

  var layersBase = [];                                              // Layers de base
  var layers = [];                                                  // layers para dce
  var polygons = [];
  var markers = [];
  var container = __params.container || "map-container" ; // contenedor del mapa:
  // initialize the map
  om.init = function( ){
    omapo = L.map( container );
    // Limite Dentro de la Republica Mexicana
    omapo.fitBounds([
        [32.7220212408026,-118.4549943762],
        [14.5330668763, -86.7124891038 ]
    ]);

    // load a tile layer
      var settings = {
        minZoom: 5,
        maxZoom: 20,
        layers: 'ENTIDAD,DISTRITO,DISTRITO_LOCAL,MUNICIPIO,SECCION,MANZANA,VIALIDAD,CARRETERAS,RIOS,LAGOS',
        format: 'image/png',
        transparent: true,
        attribution: "Dirección de Cartografía Electoral",
        version: '1.1.1',
        cache: 'proceso',
        resource: 'proceso',
        schema: 'y2018',
        // bucket: '<?php echo( $carto_cache ); ?>',
        // resource: '<?php echo( $carto_db ); ?>',
        // schema: '<?php echo( $carto_schema ); ?>',
      };
      L.tileLayer('https://{s}.google.com/vt/lyrs=s,h&x={x}&y={y}&z={z}',{
          maxZoom: 20,
          subdomains:['mt0','mt1','mt2','mt3']
      }).addTo( omapo );
      /*
      Note the difference in the "lyrs" parameter in the URL:
      Hybrid: s,h;
      Satellite: s;
      Streets: m;
      Terrain: p;
      */
     // var settings = $.extend( {}, defaults, l.options );
     // L.tileLayer.wms( 'https://172.19.1.16/global/wms/', settings ).addTo( omapo ).bringToFront();

      omapo.on('click',
        function( e ){
          var fp_mouseover = window['mapa_click'];
          if( typeof( fp_mouseover ) != 'undefined' && typeof( fp_mouseover ) === 'function' )
            fp_mouseover( e, indice );
        }
      );

  };
  om.addPolygon = function( location , __style = {} ){
    var poligono;
    var coordenadas = Array();
    for(llave1 in (location.coordinates)){
      var obj = (location.coordinates)[llave1];
      for(llave2 in obj ){
        var obj2 = obj[llave2];
        for(llave3 in obj2){
          if ( Array.isArray( obj2[llave3] ) ) // si de alguna manera ya esta decodificado lo usamos como viene
            (location.coordinates)[llave1][llave2][llave3] = obj2[llave3];
          else // si aun tiene la codificacion, haremos la decodificacion
            (location.coordinates)[llave1][llave2][llave3] = L.PolylineUtil.decode( obj2[llave3] );
          coordenadas.push( (location.coordinates)[llave1][llave2][llave3] );
        };
      };polygons
    };
    polygons.push( L.polygon( coordenadas , __style ).addTo(omapo) );
    let indice = ( polygons.length - 1 );
    // aqui ponemos los eventos del poligono:
    // for( var i = 0; i < polygons[indice].length; i++ ){
      // ** Eventos
      polygons[indice].on( {
        click: function( e ){
          // var fp_click = window['mapa_click_polygon'];
          var fp_click = __params.onpolygonclick;
          if( typeof( fp_click ) != 'undefined' && typeof( fp_click ) === 'function'  )
            fp_click( e, indice );
        },
        mouseover: function( e ){
          var fp_mouseover = __params.onpolygonmouseover;
          if( typeof( fp_mouseover ) != 'undefined' && typeof( fp_mouseover ) === 'function' )
            fp_mouseover( e, indice );
        },
        mouseout: function( e ){
          // var fp_mouseout = window['mapa_mouseout_polygon'];
          var fp_mouseout = __params.onpolygonmouseout;
          if( typeof( fp_mouseout ) != 'undefined' && typeof( fp_mouseout ) === 'function' )
            fp_mouseout( e, indice );
        }
      } );
    // }
    return indice;
  };
  om.zoomBounds = function( _data ){
    omapo.fitBounds([
        [ _data.x1 , _data.y1 ],
        [ _data.x2 , _data.y2 ]
    ]);
  };
  om.zoomtopais = function( ){
    omapo.fitBounds([
        [32.7220212408026,-118.4549943762],
        [14.5330668763, -86.7124891038 ]
    ]);
  };
  om.zoomto = function( id ){
    let poligono = findpolygon( id );
    omapo.fitBounds( poligono.getBounds() );
  };
  om.zoomPolygroup = function( ){
    omapo.fitBounds( L.featureGroup( polygons ).getBounds() );
  };
  om.addControl = function( nombre_id , _position_ ){
    // 'topleft', 'topright', 'bottomleft' or 'bottomright'
    var control = new L.Control( { position : _position_ } );
    control.onAdd = function(map) {
            var _div = L.DomUtil.create( 'div', 'map-control-box' );
            _div.innerHTML = '<div id="' + nombre_id.toString() + '"></div>';
            return _div;
    };
    control.addTo( omapo );
    return control;
  };
  om.zoomtoent = function( __entidad ){
    let poligono = findpolygon( __entidad );
    omapo.fitBounds( poligono.getBounds() );
  };
  om.hideall = function(){
    for(key in (info.entidades)){
      element = info.entidades[key];
      element.poligono.setStyle( info.estilos.hidden );
    };
    for(key in (info.distritos)){
      element = info.distritos[key];
      element.poligono.setStyle( info.estilos.hidden );
    };
  };

  om.setStyleToPolygonALL = function( style ){
    for(key in polygons ){
      polygons[key].setStyle( style );
    };
  };

  om.setStyleToPolygon = function( id , style ){
      let poligono = findpolygon( id );
      if ( poligono !== false )
        poligono.setStyle( style );
  };

  om.showall = function(){
    let style;
    style = info.estilos.entidad;
    for(key in (info.entidades)){
      element = info.entidades[key];
      element.poligono.setStyle( style );
    };
      style = info.estilos.distrito;
    for(key in (info.distritos)){
      element = info.distritos[key];
      element.poligono.setStyle( style );
    };
  };
  om.showentidad = function( __entidad ){
    let style;
    style = info.estilos.entidad;
    findpolygon( __entidad ).setStyle( style );
    style = info.estilos.distrito;

    for(key in (info.distritos)){
      element = info.distritos[key];
      if ( parseInt( element.entidad) == parseInt( __entidad) ){
        element.poligono.setStyle( style );
      }
    };
    om.zoomto( __entidad );
  };
  om.destacaDtto = function( __entidad, __distrito , __destaca ){
    if ( __destaca )
      style = info.estilos.active;
    else
      style = info.estilos.distrito;
    findpolygond( __entidad , __distrito ).setStyle( style );

  };
  function findpolygon( index ){
    if ( polygons.length > index )
      return polygons[ index ];
    else
      return false
  };
  om.removeAllPolygons = function(){
    for(key in ( polygons )){
      polygons[ key ].remove();
    };
    polygons = Array();
  };
  om.removeAllMarkers = function(){
    for(key in ( markers )){
      markers[ key ].remove();
    };
    markers = Array();
  };
  function findpolygond( __entidad , __distrito){
    for(key in (info.distritos)){
      element = info.distritos[key];
      if ( ( parseInt( element.entidad) == parseInt( __entidad) ) && ( parseInt( element.distrito) == parseInt( __distrito) ) ){
        return info.distritos[key].poligono;
      }
    };
  };
  // añadimos un marcador
  om.addMarker = function( __location , __params ){
    var location = L.latLng( __location );
    var marker = L.marker( location , {
       draggable: __params.draggable || false,
      icon: __params.icon || new L.Icon.Default ,
      autoPan: __params.autoPan || null,
      opacity: __params.opacity || null,
      index: __params.index || null
    } );
    polygons.push( marker.addTo( omapo ) );
    return marker;
  };
  return om;
};

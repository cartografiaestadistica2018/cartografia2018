var Cartografia2018 = function( __params ){
  let ot = {};
  var mapa; // objeto del mapa
  var estilos; // los estilos que se usaran segun los recursos.
  var poligonos = Array(); // aqui van el arreglo de los poligonos
  var params = __params;
  var oi = {};
  var eventos = {};
  ot.init = function( _data ){
    let error = false;
    if ( 'ObjetoIntercambio' in _data ){
      if ( typeof ( _data.ObjetoIntercambio.anio ) == "undefined" )
        error = true;
      if ( typeof ( _data.ObjetoIntercambio.tipo ) == "undefined" )
        error = true;
      if ( typeof ( _data.ObjetoIntercambio.colores ) == "undefined" )
        error = true;
      if ( typeof ( _data.ObjetoIntercambio.tematico ) == "undefined" )
        error = true;
      if ( error ){
        console.log('El objeto de intercambio requiere los valores de anio, tipo, colores y tematico', { anio : 2018, tipo : 1, colores : [ { color : "#000000" } ], tematico: [ { idcolor : 0 } ] } );
        return false;
      }
    }
    // esta es la referencia del objeto exterior al objeto interior
    params.onpolygonclick = ot.onpolygonclick;
    params.onpolygonmouseout = ot.onpolygonmouseout;
    params.onpolygonmouseover = ot.onpolygonmouseover;
    if ( typeof( mapa ) === "undefined" ){
      mapa = new LeafLetMap( params );
      mapa.init( );
    }
    // estos son los eventos del programador, que seran llamados por el objeto exterior
    eventos.onpolygonclick = _data.onpolygonclick;
    eventos.onpolygonmouseout = _data.onpolygonmouseout;
    eventos.onpolygonmouseover = _data.onpolygonmouseover;
    // Generamos una Copia Sin referencias del Objeto Original.
    oi = JSON.parse( JSON.stringify( _data.ObjetoIntercambio ) );

    // Aqui vamos a revisar que tengamos el default de resource
    if ( typeof( params.resource ) === 'undefined' )
      params.resource = "resources/json/";

    let param = {
      url : params.resource + 'estilopoligonos.json'
    };
    iu.ajax( param , function( data ){
      console.log("estilos", data);
      estilos = data;
      ot.loadCartografia( );
    } );

  };

  ot.loadCartografia = function( ){
    let param = {
      url : ''
    };
    oi.tipo = parseInt( oi.tipo );
    oi.entidad = parseInt( oi.entidad );
    console.log("tematico [a|e|t]", oi.anio , oi.entidad, oi.tipo  );
    switch (  oi.tipo ) {
      case 0: // aqui la llamada es por todo el pais
        param.url = params.resource + oi.anio + '/pais/p-0.json';
      break;
      case 1: // Aqui el Nivel es por la entidad (todas o solo una)
        param.url = params.resource + oi.anio + '/entidad/e-' + oi.entidad + '.json';
      break;
      case 2: // Aqui la Llamada es por el distrito federal (todos o solo uno)
        param.url = params.resource + oi.anio + '/distritofederal/df-' + oi.entidad + '.json';
      break;
      case 3: // Aqui la Llamada es por el distrito local (todos o solo uno)
        param.url = params.resource + oi.anio + '/distritolocal/dl-' + oi.entidad + '.json';
      break;
      case 4: // Nivel 4 son los Municipios
        param.url = params.resource + oi.anio + '/municipio/m-' + oi.entidad + '.json';
      break;
      case 5: // Nivel 5 son las secciones
        param.url = params.resource + oi.anio + '/seccion/s-' + oi.entidad + '.json';
      break;
      case 6: // Nivel 6 son las circunscripciones
        param.url = params.resource + oi.anio + '/circunscripcion/c-0.json';
      break;
      case 7: // Nivel 7 es el mundo entero Mwahahahaha
        param.url = params.resource + oi.anio + '/mundo/w-0.json';
        oi.entidad = 34;
      break;
      case 8: // Nivel 8 es Los Estados Unidos de America
        param.url = params.resource + oi.anio + '/states/st-0.json';
        oi.entidad = 33;
      break;
      default:
        let txt = "No hay un tipo valido de nivel cartografico";
        console.log( txt );
        alert( txt );
        return false;
    }
    iu.ajax( param , drawPoligonos );
  };

  function drawPoligonos( data ){
    mapa.removeAllPolygons();
    mapa.removeAllMarkers();

    if ( typeof( data.pais ) !== "undefined" )
      poligonos = data.pais;

    if ( typeof( data.entidad ) !== "undefined" )
      poligonos = data.entidad;

    if ( typeof( data.distrito_f ) !== "undefined" )
      poligonos = data.distrito_f;

    if ( typeof( data.distrito_l ) !== "undefined" )
      poligonos = data.distrito_l;

    if ( typeof( data.municipio ) !== "undefined" )
      poligonos = data.municipio;

    if ( typeof( data.mgs ) !== "undefined" )
      poligonos = data.mgs;

    if ( typeof( data.circunscripcion ) !== "undefined" )
      poligonos = data.circunscripcion;

    if ( typeof( data.paises ) !== "undefined" )
      poligonos = data.paises;

    if ( typeof( data.states ) !== "undefined" )
      poligonos = data.states;

    for(key in poligonos ){
      element = poligonos[key];
      // Aqui realizamos la revision de los parametros.
      // aqui podemos usar distrito_f distrito_l y municipio
      if ( typeof( oi.distrito_f ) !== "undefined" )
        if ( element.distrito_f != oi.distrito_f )
          continue;
      if ( typeof( oi.municipio ) !== "undefined" )
        if ( element.municipio != oi.municipio )
          continue;
      if ( typeof( oi.distrito_l ) !== "undefined" )
        if ( element.distrito_l != oi.distrito_l )
          continue;
      if ( typeof( oi.seccion ) !== "undefined" )
        if ( element.seccion != oi.seccion )
          continue;
      let poligono_identificador = mapa.addPolygon( element.location ,  estilos.poligono );
      element.poligono = poligono_identificador;
      // si existe la propiedad entidad, entonces debe tener nombre
      if ( 'entidad' in element )
        element.nentidad = iu.getEntidad( element.entidad ).name;
      else
        element.nentidad = iu.getEntidad( 0 ).name;
      poligonos.push( element );
    };

    // esto solo va a aplicar cuando haya entidad, si no hay el default debe ser 0
    let __extend = Array();
    if ( typeof( oi.entidad ) !== "undefined" ){
      if ( oi.entidad >= 0 && oi.entidad < 33 ){
        // L.featureGroup([marker1, marker2, polyline])
        mapa.zoomPolygroup();
      }else{
        // este es para el caso de USA
        __extend = iu.getEntidad( oi.entidad ).extent.split(',');
        mapa.zoomBounds( {
          x1 : __extend[1] ,
          y1 : __extend[0] ,
          x2 : __extend[3] ,
          y2 : __extend[2]
        });
      }
    }
    drawTematico();
  };

  function drawTematico( data ){

    for( key in ( oi.tematico ) ){
      let __objeto = oi.tematico[key];
      for( llave in ( poligonos ) ){
        if ( parseInt( __objeto.entidad ) === parseInt( poligonos[llave].entidad ) ){

          if ( typeof ( __objeto.seccion ) !== "undefined" ){
            if ( parseInt( __objeto.seccion ) === parseInt( poligonos[llave].seccion ) ){
              drawAsociacionTematicoPoligono( llave , __objeto );
            }
            continue;
          }

          if ( typeof ( __objeto.distrito_f ) !== "undefined" ){
            if ( parseInt( __objeto.distrito_f ) === parseInt( poligonos[llave].distrito_f ) ){
              drawAsociacionTematicoPoligono( llave , __objeto );
            }
            continue;
          }

          if ( typeof ( __objeto.municipio ) !== "undefined" ){
            if ( parseInt( __objeto.municipio ) === parseInt( poligonos[llave].municipio ) ){
              drawAsociacionTematicoPoligono( llave , __objeto );
            }else{
              continue;
            }
          }

          if ( typeof ( __objeto.distrito_l ) !== "undefined" ){
            if ( parseInt( __objeto.distrito_l ) === parseInt( poligonos[llave].distrito_l ) ){
              drawAsociacionTematicoPoligono( llave , __objeto );
            }else{
              continue;
            }
          }
          // Aqui ya paso por todas las opciones de municipio distrio y seccion, podemos asumir que si no es nunguno de los anteriores entonces debe ser pintado
          drawAsociacionTematicoPoligono( llave , __objeto );
          break;
        }else{ // no tiene entidad el poligono
          // verificar si es un pais o una circunscripcion, primero verificamos la circunscripcion
          if ( typeof ( __objeto.circunscripcion ) !== "undefined" ){
            if ( parseInt( __objeto.circunscripcion ) === parseInt( poligonos[llave].circunscripcion ) ){
              drawAsociacionTematicoPoligono( llave , __objeto );
            }else{
              continue;
            }
          }else{ // ya sabemos que no es una circunscripcion, entonces podria ser un pais
            if ( typeof ( __objeto.pais ) !== "undefined" ){
              if ( ( __objeto.pais ) === ( poligonos[llave].pais ) ){
                drawAsociacionTematicoPoligono( llave , __objeto );
              }else{
                continue;
              }
            }else{ // no es ni circunscripcion ni pais, entonces debes ser un estado de la union americana
              if ( typeof ( __objeto.state ) !== "undefined" ){
                if ( ( __objeto.state ) === ( poligonos[llave].state ) ){
                  drawAsociacionTematicoPoligono( llave , __objeto );
                }else{
                  continue;
                }
              }else{ // no es ni circunscripcion ni pais ni estado de la union americana ¿que eres?
                continue;
              }
            }
          }
          continue;
        }
      }
    }

  };
  function drawAsociacionTematicoPoligono( indice , __obj ){
    // podriamos verificar que el indice este dentro del rango.
    poligonos[ indice ].tematico = __obj;

    // el color debe existir en el array de lo contrario no debemos pintarlo, seria un error.
    if ( ( __obj.idcolor < 0 ) || ( __obj.idcolor >= oi.colores.length ) )
      return;

    let style = { };
    if ( typeof ( oi.colores[ __obj.idcolor ].color ) !== "undefined" )
      style.fillColor = oi.colores[ __obj.idcolor ].color;
    if ( typeof ( oi.colores[ __obj.idcolor ].border ) !== "undefined" )
      style.color = oi.colores[ __obj.idcolor ].border;

    mapa.setStyleToPolygon( poligonos[ indice ].poligono , style );
  };
  function find( indice__ ){
    for( llave in ( poligonos ) ){
      if ( parseInt( indice__ ) === parseInt( poligonos[llave].poligono ) ){
        return poligonos[llave];
        break;
      }else{
        continue;
      }
    }
  };
  // Eventos SobreCargados
  ot.onpolygonclick = function( e , indice ){
    // Aqui los eventos asignados al poligono en cuestion
    let infopoligono = { e: e, indice : indice, info : find( indice ) };
    if (typeof eventos.onpolygonclick === "function")
      eventos.onpolygonclick( infopoligono );
  };
  ot.onpolygonmouseout = function( e , indice ){
    // Aqui los eventos asignados al poligono en cuestion
    let infopoligono = { e: e, indice : indice, info : find( indice ) };    
    if (typeof eventos.onpolygonmouseout === "function"){
      eventos.onpolygonmouseout( infopoligono );
      mapa.setStyleToPolygon( poligonos[ indice ].poligono , estilos.inactivo );
    }
  };
  ot.onpolygonmouseover = function( e , indice ){
    // Aqui los eventos asignados al poligono en cuestion
    let infopoligono = { e : e, indice : indice, info : find( indice ) };
    if (typeof eventos.onpolygonmouseover === "function"){
      eventos.onpolygonmouseover( infopoligono );
      mapa.setStyleToPolygon( poligonos[ indice ].poligono , estilos.activo );
    }
  };
  ot.addControl = function( nombre_id , _position_ ){
    mapa.addControl(nombre_id, _position_ );
  };
  // aqui regresamos el objeto, La parte visible del codigo para el navegador
  return ot;
};


var InterfazUsuario = function( ){

  var or = {};

  function funcion( e ){
    // evitamos acciones por default
    e.preventDefault();
  };
  or.ajax = function( Ainf , call_back ){

    Ainf.options = {
      cache: false,
      async: true,
      method: "POST",
      data: Ainf.data
    };

    Ainf.url = Ainf.url.toString();
    if ( Ainf.url.startsWith("http") ){
      Ainf.url = Ainf.url;
    }else{
      Ainf.url = '' + Ainf.url;
    }

    $.ajax( Ainf.url, Ainf.options )
      .then( function( data ){

        if ( typeof( data ) == "undefined" ){
          if( call_back != null ){
            call_back( data );
          }
        }else{
          if ( ( typeof( data.status ) != "undefined" ) ){
            if( call_back != null ){
              call_back( data );
            }
          }else {
            if( call_back != null ){
              call_back( data );
            }
          }
        }

    } );

  };

  or.iframePost = function( url, options, value__ ){
      //! devuelve un objeto para realizar respuestas POST a un iframe
      var o = {};
      var defaults = {
          id: new Date().getTime(),
          box: 'body',
          style: 'style="display:block"'
      };
      var settings = $.extend( {}, defaults, options );
      $( '#' + settings.box ).append('<iframe data-id="' + settings.id.toString() + '" name="ui_iframe_' + settings.id.toString() + '" id="ui_iframe_' + settings.id.toString() + '" ' + settings.style + ' ></iframe>');

      o.form = $( '<form action="' + url.toString() + '" target="ui_iframe_' + settings.id.toString() + '" method="POST" id="ui_form_' + settings.id.toString() + '" ' + settings.style + ' ></form>' );

      $( '#' + settings.box ).append( o.form );
      $( o.form).append( o.form );
      $( '<input type="hidden" />' )
          .attr( 'name', 'json' )
          .attr( 'value', value__ )
          .appendTo( o.form );

      o.form.submit();

      return o;
  };
  or.getEntidad = function( param ){
    let entidad = [
        {id:0, name: "México", extent:"-118.207261,33.207261,-86.5022,13.994276"},
        {id:1, name: "Aguascalientes", extent:"-102.893,21.6216,-101.845,22.4598"},
        {id:2, name: "Baja California", extent:"-118.455,28.0001,-112.766,32.722"},
        {id:3, name: "Baja California Sur", extent:"-115.224,22.871,-109.412,28.0005"},
        {id:4, name: "Campeche", extent:"-92.4676,17.8137,-89.1092,21.2366"},
        {id:5, name: "Coahuila", extent:"-103.986,24.5353,-99.8114,29.8777"},
        {id:6, name: "Colima", extent:"-111.057,18.6848,-103.487,19.5088"},
        {id:7, name: "Chiapas", extent:"-94.2325,14.5331,-90.371,17.9952"},
        {id:8, name: "Chihuahua", extent:"-109.072,25.5593,-103.302,31.7878"},
        {id:9, name: "Ciudad de México", extent:"-99.3742,19.0495,-98.9456,19.5938"},
        {id:10, name: "Durango", extent:"-107.219,22.3459,-102.475,26.8347"},
        {id:11, name: "Guanajuato", extent:"-102.098,19.9164,-99.6692,21.8395"},
        {id:12, name: "Guerrero", extent:"-102.184,16.3217,-98.0655,18.8882"},
        {id:13, name: "Hidalgo", extent:"-99.8541,19.5989,-97.9851,21.3985"},
        {id:14, name: "Jalisco", extent:"-105.697,18.92,-101.513,22.7505"},
        {id:15, name: "México", extent:"-100.612,18.368,-98.6052,20.2852"},
        {id:16, name: "Michoacán", extent:"-103.74,17.9134,-100.081,20.3946"},
        {id:17, name: "Morelos", extent:"-99.4944,18.3324,-98.6294,19.1337"},
        {id:18, name: "Nayarit", extent:"-106.69,20.6046,-103.717,23.0769"},
        {id:19, name: "Nuevo León", extent:"-101.168,23.1637,-98.4173,27.7995"},
        {id:20, name: "Oaxaca", extent:"-98.5641,15.6565,-93.8684,18.6589"},
        {id:21, name: "Puebla", extent:"-99.069,17.863,-96.7247,20.84"},
        {id:22, name: "Querétaro", extent:"-100.597,20.0132,-99.0466,21.6926"},
        {id:23, name: "Quintana Roo", extent:"-89.4185,17.8136,-86.7125,21.609"},
        {id:24, name: "San Luis Potosí", extent:"-102.29,21.1605,-98.3323,24.4915"},
        {id:25, name: "Sinaloa", extent:"-109.45,22.4718,-105.368,27.078"},
        {id:26, name: "Sonora", extent:"-115.05,26.3034,-108.453,32.4935"},
        {id:27, name: "Tabasco", extent:"-94.1298,17.2513,-90.9833,18.6532"},
        {id:28, name: "Tamaulipas", extent:"-100.145,22.2071,-97.1424,27.6814"},
        {id:29, name: "Tlaxcala", extent:"-98.7226,19.1048,-97.6274,19.7287"},
        {id:30, name: "Veracruz", extent:"-98.6924,17.1211,-93.6133,22.4723"},
        {id:31, name: "Yucatán", extent:"-90.4052,19.5294,-87.3654,21.625"},
        {id:32, name: "Zacatecas", extent:"-104.328,21.042,-100.758,25.2019"},
        {id:33, name: "USA", extent:"-178.2175,18.9247,-66.969,71.406"} ,
        {id:34, name: "MUNDO", extent:"-115,-48,115,60"}];
      entidad = entidad[ param ];
    return entidad;
  };
  return or;
};

window.iu = new InterfazUsuario();

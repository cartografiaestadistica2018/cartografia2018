**Organizacion de Geojson para Cartografia**

Documento que describe la Cartografia
El arbol de carpetas se organiza de la siguiente forma \corte\capa\archivo.json
*Informacion que cura*

---

## Corte

Aqui vamos a poner los diferentes cortes disponibles por año (en virtud de que este producto va a ser un historico)

---

## Capa

1. entidad
2. distrito_federal
3. distrito_local
4. municipio
5. seccion

---

## GeoJson

[Codificacion de Polilineas](https://developers.google.com/maps/documentation/utilities/polylineutility).
